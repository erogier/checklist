//
//  ViewController.swift
//  checklist
//
//  Created by Eddy Rogier on 10/10/2017.
//  Copyright © 2017 Eddy R. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    // MARK: - var and let
    let identifiant = "checklistCell"
    var liste:Liste?
    
    // MARK: - CoreData
    var defaults = UserDefaults.standard
    
    
    // MARK: - outlet
    @IBOutlet weak var checkListTableView: UITableView!
    
    // MARK: - Start
    override func viewDidLoad() {
        super.viewDidLoad()
        checkListTableView.dataSource = self
        checkListTableView.delegate = self
        
//        let listeItems =  telechargerItems()
//        liste = Liste(items:listeItems)
        liste = Liste(items: [])
        telechargerEtRafraichir()
        
        
        
    }
    
    // MARK: - gestion ITEM
    func afficher() -> [Item]{
        var items:[Item] = []
        for i in 0...10 {
            let item = Item(texte: "item \(i)")
            items.append(item)
        }
        return items
    }
    
    // MARK: - datasource methode
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return liste!.items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: identifiant, for: indexPath)
        
        let item = liste!.items[indexPath.row]
        cell.textLabel!.text = item.texte
    
        item.changerStyleCelluleSiStatutFait(item: item, cell: cell)
        
        return cell
    }
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			liste!.supprimer(index: indexPath.row)
			tableView.deleteRows(at: [indexPath], with: .bottom)
			
			liste?.sauvegarderItems(items: liste!.items, defaults: defaults)
			telechargerEtRafraichir()
		} else {
			
		}
	}
    
    
    
    // MARK: - delegate methode
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("index : \(indexPath.row)")
        let item = liste!.items[indexPath.row]
        
        item.changerStatut()
        liste?.sauvegarderItems(items: (liste?.items)!, defaults: defaults)
        telechargerEtRafraichir() 
        
        checkListTableView.reloadData()
    }
    
}

extension ViewController {
    // MARK: - AlertDialog
    func alertDialog(){
        let alert = UIAlertController(title: "Ajouter Item", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let annulerAction = UIAlertAction(title: "Annuler", style: .cancel) { (action) in
            print("action annuler")
        }
        
        let ajouterAction = UIAlertAction(title: "Ajouter", style: .default) { (action) in
            //MARK - 2 quand je cliquerais sur ajouter depuis la modale ʕ•ᴥ•ʔ
            let itemText = alert.textFields![0]
            //self.ajouterListItem(text: itemText.text!)
            self.liste!.creerItem(item: itemText.text!, defaults:self.defaults)
            //pour mettre a jour le container
            self.telechargerEtRafraichir()
        }
        
        alert.addTextField { (textfield) in
            textfield.placeholder = "item"
        }
        alert.addAction(annulerAction)
        alert.addAction(ajouterAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    
    
    
    
    
    @IBAction func ajouterItemAction(_ sender: UIBarButtonItem) {
        print("ajouter")
        alertDialog()
    }

    func ajouterListItem(text:String) {
        let item = Item(texte: "texte") // je crée un nouvle item ʕ•ᴥ•ʔ
        //liste?.items.append(item)
        // pour mettre a jour le container view ʕ•ᴥ•ʔ
        liste!.ajouterItem(item: item)
        checkListTableView.reloadData() // important
    }

   // creer et telecharger
    
    func telechargerEtRafraichir(){
        self.liste!.items = self.liste!.telechargerItems(defaults: defaults)
        self.checkListTableView.reloadData()
    }
}























