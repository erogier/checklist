//
//  List.swift
//  checklist
//
//  Created by Eddy Rogier on 10/10/2017.
//  Copyright © 2017 Eddy R. All rights reserved.
//

import Foundation

class Liste {
    var items:[Item]
    
    init(items:[Item]) {
        self.items = items
    }
    
    func ajouterItem(item:Item){
        self.items.append(item)
    }
    
    func supprimer(index:Int){
        self.items.remove(at: index)
    }
}
