//
//  Liste+default.swift
//  checklist
//
//  Created by Eddy Rogier on 11/10/2017.
//  Copyright © 2017 Eddy R. All rights reserved.
//

import Foundation
import UIKit
// ces methodes concernent la liste ʕ•ᴥ•ʔ 
extension Liste {
    
    // MARK: - methode UserDefault
    func creerItem(item:String ,defaults:UserDefaults){
        var itemsSauvegardes:[Dictionary] = [Dictionary<String,Any>]()
        let nouvoitem = ["texte": item, "status":0] as [String : Any]
        
        // on verifie si il y a des existant dan userdefault
        if let items = defaults.object(forKey: "items") as? [Dictionary<String, Any>] {
            // les existant
            itemsSauvegardes = items
        }
        // nouvelle item ajouté
        itemsSauvegardes.append(nouvoitem)
        defaults.set(itemsSauvegardes, forKey: "items")
    }
    
    func sauvegarderItems(items:[Item], defaults:UserDefaults){
        var itemsSauvegardes:[Dictionary<String, Any>] = []
       
        for item in items {
            let i = ["texte": item.texte, "status": setStatus(item: item)] as [String : Any]
            itemsSauvegardes.append(i)
        }
        defaults.set(itemsSauvegardes, forKey: "items")
        
    }
    
    func telechargerItems(defaults:UserDefaults) -> [Item]{
        var items:[Item] = []
        
        if let itemsArray = defaults.object(forKey: "items") as? [Dictionary<String, Any>] {
            
            for item in itemsArray {
                let i = Item(texte: item["texte"] as! String)
				i.statutFait = getStatut(itemValue: item["status"] as! Int)
                items.append(i)
            }
        }
        return items
    }
    
	
    
    func setStatus(item:Item) -> Int {
        let statutValue = item.statutFait ? 1 : 0
        return statutValue
    }
    
    func getStatut(itemValue:Int) -> Bool {
        let statut = itemValue == 1 ? true : false
        return statut
    }
    
    //\/ -------------------------
}
