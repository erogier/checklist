//
//  item+Formattage.swift
//  checklist
//
//  Created by Eddy Rogier on 11/10/2017.
//  Copyright © 2017 Eddy R. All rights reserved.
//

import Foundation
import UIKit

extension Item {
    
    public func changerStyleCelluleSiStatutFait(item:Item, cell:UITableViewCell){
//        if item.statutFait {
//            surligner(texte: item.texte,  cell)
//        } else {
//            desurligner(texte: item.texte, cell)
//        }
        
        item.statutFait ? surligner(texte: item.texte,  cell) : desurligner(texte: item.texte, cell)
        cell.accessoryType = item.statutFait ? .checkmark : .none
    }
    
    private func surligner(texte: String,_ cell:UITableViewCell) {
        var stringFormatted:NSAttributedString?
        
        let attributs = [
            NSAttributedStringKey.foregroundColor       : UIColor.orange,
            NSAttributedStringKey.strikethroughStyle    : NSNumber(value : NSUnderlineStyle.styleSingle.rawValue)
        ]
        
        stringFormatted = NSAttributedString(string: texte, attributes: attributs)
        cell.textLabel?.attributedText = stringFormatted!
        //return stringFormatted!
    }
    
   private func desurligner(texte: String,_ cell:UITableViewCell) {
        var stringFormatted:NSAttributedString?
        
        let attributs = [
            NSAttributedStringKey.foregroundColor       : UIColor.darkText,
            //NSAttributedStringKey.strikethroughStyle    : NSNumber(value : NSUnderlineStyle.styleNone.rawValue)
            NSAttributedStringKey.strikethroughStyle    : NSNumber(value : 0)
        ]
        stringFormatted = NSAttributedString(string: texte, attributes: attributs)
        cell.textLabel?.attributedText = stringFormatted!
     //   return stringFormatted!
    }
}
