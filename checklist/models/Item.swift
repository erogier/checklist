//
//  Item.swift
//  checklist
//
//  Created by Eddy Rogier on 10/10/2017.
//  Copyright © 2017 Eddy R. All rights reserved.
//

import Foundation

class Item {
    var texte:String
    var statutFait:Bool
    
    init(texte:String) {
        self.texte = texte
        self.statutFait = false
    }
    
    func changerStatut(){
        
        self.statutFait = self.statutFait ? false : true
    }
}
